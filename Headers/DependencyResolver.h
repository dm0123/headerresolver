#ifndef __DEPENDENCY_RESOLVER_H__
#define __DEPENDENCY_RESOLVER_H__

#include <map>
#include <memory>

#include "HeaderParser.h"
#include "FileResolver.h"

namespace HR
{
	typedef std::map<std::string, std::pair<includes_vector, includes_vector>> dependency_map;
	typedef std::map<std::string, int> dependency_statistics;

	typedef std::unique_ptr<FileResolver> files_ptr;
	typedef std::unique_ptr<HeaderParser> parser_ptr;

	class DependencyResolver
	{
		static const std::string headerExtensions;
		static const std::string sourceExtensions;

	public:
		DependencyResolver(const std::string& srcPath, const std::string& includePath, const std::string& libPath);

		/**
		 * \brief High-level dependency resolve
		 */
		void processData();

		/**
		 * \brief Returns result of dependency search
		 */
		const dependency_map& getDependencyMap() const { return dependencies; }

		/**
		 * \brief Returns statistics of dependency search
		 */
		const dependency_statistics& getStatistics() const { return stats; }

		/**
		 * \brief Outputs formatted dependency tree into string
		 */
		std::string getFormattedDependencyTree();

		/**
		 * \brief Outputs formatted statistics into string
		 */
		std::string getFormattedStats();

	private:
		dependency_map dependencies;
		dependency_statistics stats;

		files_ptr files;
		parser_ptr parser;

		std::string srcPath;
		std::string includePath;
		std::string libPath;

		filename_vector srcIncludeFiles;
		filename_vector libIncludeFiles;
		filename_vector srcFiles;
		
		/**
		 * \brief Recursive search. Each call tries to open file and tries to parse contents if open was successfull
		 */
		void searchDependencyInFile(const std::string& fileName);

		/**
		 * \brief Builds text representation of dependency tree
		 */
		void buildTreeRecursive(const std::string& fileName, std::stringstream& output, int& dotsCount);
	};
}

#endif // __DEPENDENCY_RESOLVER_H__