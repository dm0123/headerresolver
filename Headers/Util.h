#ifndef __HR_UTIL_H__
#define __HR_UTIL_H__

#include <cstring>
#include <string>
#include <vector>
#include <sstream>

namespace string_util
{
	typedef std::vector<std::string> string_vector;
	
	/* Copied from my pet project */
	/**
	* \brief Method splits string by 1 character
	* \param text Input text to split
	* \param splitChar Deliminator character
	* \return vector of strings
	*/
	inline string_vector split(const std::string& text, char splitChar)
	{
		string_vector result;
		std::stringstream stream;
		stream.str(text);
		std::string item;

		while (std::getline(stream, item, splitChar))
		{
			result.push_back(item);
		}

		return result;
	}

	typedef std::vector<std::string> command_line_args;

	/**
	 * \brief Function extracts command line arguments for program.
	 */
	inline command_line_args parse_command_line_args(int argc, char** argv)
	{
		// TODO: handle paths with whitespaces
		const std::string error = "Invalid parameters. Usage is: <src path> [options]";
		std::vector<std::string> args;
		args.reserve(3); // we know exact quantinty

		// args[1] should be path to sources
		args.push_back(argv[1]);

		for (auto i = 1; i < argc; i++)
		{
			if (i + 1 == argc)
				continue;

			if (strcmp(argv[i], "-I") == 0)
				args.push_back(std::string(argv[i + 1]));
		}

		return args;
	}
}

#endif // __HR_UTIL_H__
