#ifndef __HEADER_PARSER_H__
#define __HEADER_PARSER_H__

#include <vector>
#include <fstream>

namespace HR
{
	typedef std::vector<std::string> includes_vector;

	class HeaderParser
	{
		const int referenceWordLength = 7; // #include-directive length

	public:
		/**
		 * \brief Main parsing method. Gets all #include file names
		 * \param file_stream input file
		 * \return pair - first regular includes, second lib includes
		 */
		std::pair<includes_vector, includes_vector> getHeaderUsage(std::stringstream& file_stream);

#ifdef _TESTING 
	public:
#else
	private:
#endif
		/**
		 * \brief Counts how much sequence appeared in line from initialPosition to end
		 */
		int countSequenceMatchesFrom(std::string line, std::string sequence, int initialPosition=-1);

		/**
		 * \brief Counts how much sequence appeared in line from start to initialPosition
		 */
		int countSequenceMatchesTo(std::string line, std::string sequence, int initialPosition=-1);

		/**
		 * \brief Is sequense from startPos to endPos inside non-inline comment
		 */
		bool substrIsInsideComment(const std::string& line, size_t startPos, size_t endPos, const int openedCommentStack);

		/**
		 * \brief Finds file name right after #include directive
		 * \param line Current line
		 * \param referencePos Position on the line where #include directive begins
		 * \param openedCommentStack Open symbols of non-inline comment count
		 * \param searchLibRefs Defines what we search - library reference or not
		 * \return Parsed file name
		 */
		std::string findNearestFileName(std::string line, size_t referencePos, const int openedCommentStack, bool searchLibRefs = false);
	};
}

#endif // __HEADER_PARSER_H__