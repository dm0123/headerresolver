#ifndef  __FILE_RESOLVER_H__
#define __FILE_RESOLVER_H__

#include <vector>
#include <string>

namespace HR
{
	typedef std::vector<std::string> filename_vector;

	class FileResolver
	{
		static const char extension_delim = ';';
	public:
		/**
		 * \brief Search files in filesystem by given extensions list
		 * separated by extension_delim.
		 */
		static filename_vector getFilenames(const std::string& path, const std::string& extension);

		/**
		 * \brief Read all file contents into raw buffer.
		 * This could use a lot of memory for big files.
		 */
		static std::stringstream getBufferFromFile(const std::string& fileName);

		/**
		 * \brief Gets file name from path. Wrapper for 
		 * std experimental path class filename() method.
		 */
		static std::string getFileName(const std::string& path);
	};
}

#endif // __FILE_RESOLVER_H__