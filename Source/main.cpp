/* 
 * This is c++ header resolver. It takes 3 parameters: source path,
 * include path and lib path and outputs dependencies for source files.
 */

#include <iostream>

#include "../Headers/DependencyResolver.h"
#include "../Headers/Util.h"

int main(int argc, char** argv)
{
	auto args = string_util::parse_command_line_args(argc, argv);
	if (args.size() < 3) // we know exact parameters count
	{
		std::cout << "Invalid parameters. Usage is: <src path> [options].\n";
		return 1;
	}

	try 
	{
		HR::DependencyResolver resolver(args[0], args[1], args[2]);
		resolver.processData();

		std::cout << resolver.getFormattedDependencyTree();
		std::cout << "\n";
		std::cout << resolver.getFormattedStats();
	}
	catch(...)
	{
		std::cout << "Unknown error\n";
		return 1;
	}
	
	return 0;
}
