#include "../Headers/DependencyResolver.h"
#include "../Headers/FileResolver.h"
#include "../Headers/Util.h"

#include <sstream>
#include <algorithm>

const std::string HR::DependencyResolver::headerExtensions = ".h;.hpp;.hh";
const std::string HR::DependencyResolver::sourceExtensions = ".cpp;.cc";

HR::DependencyResolver::DependencyResolver(const std::string& srcPath, const std::string& includePath, const std::string& libPath) : srcPath(srcPath), includePath(includePath), libPath(libPath)
{
	files.reset(new FileResolver);
	parser.reset(new HeaderParser);

	srcIncludeFiles = FileResolver::getFilenames(includePath, headerExtensions);
	libIncludeFiles = FileResolver::getFilenames(libPath, headerExtensions);
	srcFiles = FileResolver::getFilenames(srcPath, sourceExtensions);
}

void HR::DependencyResolver::processData()
{
	// working with dependencies
	for (auto& fileName : srcFiles)
	{
		searchDependencyInFile(srcPath + fileName);
	}
}

std::string HR::DependencyResolver::getFormattedDependencyTree()
{
	std::stringstream output;
	for (auto fileName : srcFiles)
	{
		auto dotsCount = 0;
		buildTreeRecursive(srcPath + fileName, output, dotsCount);
	}

	return output.str();
}

std::string HR::DependencyResolver::getFormattedStats()
{
	std::stringstream output;
	for (auto stat : stats)
	{
		output << stat.first << " " << stat.second << "\n";
	}

	return output.str();
}

void HR::DependencyResolver::searchDependencyInFile(const std::string& path)
{
	auto buf = FileResolver::getBufferFromFile(path);
	if (buf)
	{
		auto headers = parser->getHeaderUsage(buf);

		// search ordinary includes for file
		auto includes = headers.first;
		if (includes.size() > 0) // if found something
		{
			dependencies[path].first = includes;
			for (auto include : includes)
			{
				if (dependencies.find(includePath + include) == dependencies.end()) // if we weren't there
					searchDependencyInFile(includePath + include); // so recursion ends when no files found or all files were searched
			}
		}

		// search lib includes for file
		auto libIncludes = headers.second;
		if (libIncludes.size() > 0)
		{
			dependencies[path].second = libIncludes;
			for (auto& include : libIncludes)
			{
				if (dependencies.find(libPath + include) == dependencies.end())
					searchDependencyInFile(libPath + include);
			}
		}
	}
}

void HR::DependencyResolver::buildTreeRecursive(const std::string& path, std::stringstream& output, int& dotsCount)
{
	for (auto i = 0; i < dotsCount; i++)
	{
		output << "..";
	}

	auto fileName = FileResolver::getFileName(path);

	// update stats
	if (stats.find(fileName) == stats.end())
	{
		auto isRoot = std::find(srcFiles.begin(), srcFiles.end(), fileName) != srcFiles.end();
		stats[fileName] = isRoot ? -1 : 0; // the trick is: root files are there at least once before parse begins
	}

	stats[fileName]++;

	if (dependencies.find(path) != dependencies.end())
	{
		output << fileName << "\n"; // print just fileName
		auto fileDeps = dependencies[path];

		for (auto include : fileDeps.first) // regular includes
		{
			buildTreeRecursive(includePath + include, output, ++dotsCount);
		}

		for (auto include : fileDeps.second) // lib includes
		{
			buildTreeRecursive(libPath + include, output, ++dotsCount);
		}
	}
	else
	{
		output << FileResolver::getFileName(path) << " (!)\n"; // marking not found files
	}

	--dotsCount;
	if (dotsCount < 0) 
		dotsCount = 0;
}
