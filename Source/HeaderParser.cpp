#include "../Headers/HeaderParser.h"

#include <string>
#include <stack>
#include <sstream>

std::pair<HR::includes_vector, HR::includes_vector> HR::HeaderParser::getHeaderUsage(std::stringstream& fileBuf)
{
	auto openedCommentStack = 0;
	includes_vector includeFiles;
	includes_vector libFiles;

	// loop through lines
	for (std::string line; std::getline(fileBuf, line);)
	{
		// remove anything after first inline comment
		auto inlineCommentPos = line.find("//");
		if (inlineCommentPos != std::string::npos)
			line.erase(line.begin() + inlineCommentPos, line.end());
		
		// loop through #include's
		for (auto referencePos = line.find("#include"); referencePos != std::string::npos;)
		{
			if (!substrIsInsideComment(line, referencePos, referencePos + referenceWordLength, openedCommentStack))
			{
				// place include file name in vector
				auto includeName = findNearestFileName(line, referencePos, openedCommentStack, false);
				if (includeName != "")
					includeFiles.push_back(includeName);

				// place lib file name in vector
				auto includeLibName = findNearestFileName(line, referencePos, openedCommentStack, true);
				if (includeLibName != "")
					libFiles.push_back(includeLibName);
			}

			referencePos = line.find("#include", referencePos + 1);
		}

		// count non-inline comments
		auto openCommentsCount = countSequenceMatchesFrom(line, "/*");
		auto closeCommentsCount = countSequenceMatchesFrom(line, "*/");

		// close or open comments from preious lines
		openedCommentStack += openCommentsCount - closeCommentsCount;
		if (openedCommentStack < 0)
			openedCommentStack = 0;
	}

	return std::pair<includes_vector, includes_vector>(includeFiles, libFiles);
}

int HR::HeaderParser::countSequenceMatchesFrom(std::string line, std::string sequence, int initialPosition)
{
	auto count = 0;
	auto tempPos = line.find(sequence, initialPosition == -1 ? 0 : initialPosition);

	while (tempPos != std::string::npos)
	{
		count++;
		tempPos = line.find(sequence, tempPos + 1);
	}

	return count;
}

int HR::HeaderParser::countSequenceMatchesTo(std::string line, std::string sequence, int initialPosition)
{
	if (initialPosition > 0 && (unsigned int)initialPosition <= line.size())
		line.erase(initialPosition);

	return countSequenceMatchesFrom(std::move(line), sequence);
}

bool HR::HeaderParser::substrIsInsideComment(const std::string& line, size_t startPos, size_t endPos, const int openedCommentStack)
{
	auto openCount = openedCommentStack + countSequenceMatchesTo(line, "/*", startPos);

	return openCount > countSequenceMatchesTo(line, "*/", endPos) && 
			openCount != 0;
}

std::string HR::HeaderParser::findNearestFileName(std::string line, size_t referencePos, const int openedCommentStack, bool searchLibRefs)
{
	const auto beginChar = searchLibRefs ? "<" : "\"";
	const auto endChar = searchLibRefs ? ">" : "\"";

	auto fileNameStart = line.find(beginChar, referencePos + referenceWordLength);
	if (fileNameStart != std::string::npos)
	{
		auto fileNameEnd = line.find(endChar, fileNameStart + 1); // search after beginChar not inclusive
		if (fileNameEnd != std::string::npos)
		{
			auto fileName = line.substr(fileNameStart + 1, fileNameEnd - (fileNameStart + 1)); // without quotes
			if (!substrIsInsideComment(line, fileNameStart, fileNameEnd , openedCommentStack))
				return fileName;
		}
	}

	return "";
}
