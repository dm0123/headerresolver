#include "../Headers/FileResolver.h"
#include "../Headers/Util.h"

#include <fstream>
#include <experimental/filesystem>
#include <sstream>

namespace fs = std::experimental::filesystem::v1;

HR::filename_vector HR::FileResolver::getFilenames(const std::string& path, const std::string& extension)
{
	filename_vector output;
	fs::path pathToSearch(path);
	fs::directory_iterator end;
	auto extensions = string_util::split(extension, extension_delim);

	if (fs::exists(pathToSearch) && fs::is_directory(pathToSearch))
	{
		for (fs::directory_iterator iter(pathToSearch); iter != end; ++iter)
		{
			auto ext = (*iter).path().extension();
			if (fs::is_regular_file(*iter) && 
				std::find(extensions.begin(), extensions.end(), (*iter).path().extension()) != extensions.end())
				output.push_back((*iter).path().filename().generic_string());
		}
	}

	return output;
}

std::stringstream HR::FileResolver::getBufferFromFile(const std::string& fileName)
{
	std::stringstream buf;
	std::ifstream fileStream;

	// copy file into memory
	fileStream.open(fileName);
	buf << fileStream.rdbuf();

	if (fileStream.is_open())
		fileStream.close();

	return buf;
}

std::string HR::FileResolver::getFileName(const std::string& path)
{
	fs::path fsPath(path);
	return fsPath.filename().generic_string();
}
